<?php

namespace App\Http\Controllers\CompanyEmployee;

use App\Model\CompanyEmployee\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employee = Employee::all();
        return view('organisasi.employee.index',['employee' => $employee]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('organisasi.employee.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'nama' => 'required',
            'atasan_id' => 'required',
            'company_id' => 'required'
        ]);
        Employee::create($request->all());
        return redirect('employee')->with('status','Data Employee berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Employee   $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        return view('organisasi.employee.edit', compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Employee   $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        $request->validate([
            'id' => 'required',
            'nama' => 'required',
            'atasan_id' => 'required',
            'company_id' => 'required'
        ]);
        Employee::where('id', $employee->id)
            ->update([
                'id'=>$request->id,
                'nama'=>$request->nama,
                'atasan_id'=>$request->atasan_id,
                'company_id'=>$request->company_id
            ]);
        return redirect('employee')->with('status','Data Employee berhasil diedit!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Employee   $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        Employee::destroy($employee->id);
        return redirect('employee')->with('delete','Data Employee berhasil dihapus!');
    }
}

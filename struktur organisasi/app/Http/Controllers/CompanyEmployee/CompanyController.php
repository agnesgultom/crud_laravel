<?php

namespace App\Http\Controllers\CompanyEmployee;

use App\Model\CompanyEmployee\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company = Company::all();
        return view('organisasi.company.index',['company'=>$company]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('organisasi.company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id'=>'required',
            'nama'=>'required',
            'alamat'=>'required'
        ]);
        Company::create($request->all());
        return redirect('company')->with('status','Data company berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        return view('organisasi.company.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Company   $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        $request->validate([
            'id'=>'required',
            'nama'=>'required',
            'alamat'=>'required'
        ]);
        Company::where('id', $company->id)
            ->update([
                'id'=>$request->id,
                'nama'=>$request->nama,
                'alamat'=>$request->alamat
            ]);
        return redirect('company')->with('status','Data Company berhasil diedit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Company   $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        Company::destroy($company->id);
        return redirect('company')->with('delete', 'Data Company berhasil dihapus');
    }
}

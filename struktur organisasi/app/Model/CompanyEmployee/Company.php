<?php

namespace App\Model\CompanyEmployee;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{protected $table = 'companies';
    protected $fillable = ['id', 'nama', 'alamat'];

}

<?php

namespace App\Model\CompanyEmployee;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'employees';
    protected $fillable = ['id', 'nama', 'atasan_id', 'company_id'];
}

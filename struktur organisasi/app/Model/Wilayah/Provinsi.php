<?php

namespace App\Model\Wilayah;

use Illuminate\Database\Eloquent\Model;

class Provinsi extends Model
{
    protected $table = 'provinsi';
    protected $fillable = [
        'id', 'nama'
    ];

    public function kotakabupaten(){
        return $this->hasMany('App/Model/KotaKabupaten');
    }
}

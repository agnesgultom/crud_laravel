<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel dan React JS</title>
    <link href="{{asset('css/master.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<div class="container center container_">
    <h3 style="text-align: center">Tugas 4 CRUD Struktur Organisasi</h3>
    <div class="row vertical-align-center" style="margin-top: 30px;">
        <div class="col-sm-4">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">CRUD Employee dan Company</h5>
                    <p class="card-text">Dapat mengelolah data Employee dan Company menggunakan framework Laravel</p>
                </div>
                <div class="card-footer">
                    <a href="employee" class="btn btn-primary buttomleft">Kunjungi</a>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">CRUD Wilayah</h5>
                    <p class="card-text">
                        Mengelolah data Provinsi, Kota/Kabupaten, Kecamatan, Desa (Relasi one to many) menggunakan
                        Framework Laravel dan React
                    </p>
                </div>
                <div class="card-footer">
                    <a href="welcome" class="btn btn-primary buttomleft">Kunjungi</a>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Reservasi Film di Bioskop</h5>
                    <p class="card-text">
                        Reservasi film dengan relasi many to many antara penonton dan film.
                        Menggunakan Framework Laravel dan React JS
                    </p>
                </div>
                <div class="card-footer">
                    <a href="#" class="btn btn-primary buttomleft">Kunjungi</a>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>

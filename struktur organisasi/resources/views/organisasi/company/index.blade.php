@extends('organisasi.layout.main')
@section('title', 'Company')

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-10">
                <h1 class="mt-3">Daftar Company</h1>
                <a href="../company/create" class="btn btn-primary my-3">Tambah</a>
                @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                @endif
                @if (session('delete'))
                    <div class="alert alert-danger">
                        {{session('delete')}}
                    </div>
                @endif
                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Alamat</th>
                        <th scope="col">Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($company as $comp)
                        <tr>
                            <th>{{$comp->id}}</th>
                            <td>{{$comp->nama}}</td>
                            <td>{{$comp->alamat}}</td>
                            <td>
                                <a href="company/{{$comp->id}}/edit" class="btn btn-success">Edit</a>
                                <form action="company/{{$comp->id}}" method="post" class="d-inline">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Hapus</button>
                                </form>
                            </td>
                        </tr>
                    </tbody>
                    @endforeach
                </table>
            </div>
        </div>
    </div>

@endsection

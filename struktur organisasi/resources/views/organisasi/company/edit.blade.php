
@extends('layout.main')

@section('title', 'Edit Company')

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-10">
                <h1 class="mt-3">Form Edit Data Company</h1>
                <form method="post" action="../company/{{$company->id}}">
                    @method('patch')
                    @csrf
                    <div class="form-group">
                        <label for="id" class="form-label">ID</label>
                        <input type="number" class="form-control @error('id') is-invalid @enderror" id="id" placeholder="Masukkan ID Company" name="id"
                        value="{{$company->id}}">
                        @error('id')<div class="invalid-feedback">{{$message}}</div>@enderror
                    </div>
                    <div class="form-group">
                        <label for="nama" class="form-label">Nama</label>
                        <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" placeholder="Masukkan Nama Company" name="nama"
                        value="{{$company->nama}}">
                        @error('nama')<div class="invalid-feedback">{{$message}}</div>@enderror
                    </div>
                    <div class="form-group">
                        <label for="alamat" class="form-label">Alamat</label>
                        <input type="text" class="form-control @error('alamat') is-invalid @enderror" id="alamat" placeholder="Masukkan Alamat Company" name="alamat"
                        value="{{$company->alamat}}">
                        @error('alamat')<div class="invalid-feedback">{{$message}}</div>@enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@extends('organisasi.layout.main')

@section('title', 'Employee')

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-10">
                <h1 class="mt-3">Daftar Employee</h1>
                <a href="../employee/create" class="btn btn-primary my-3">Tambah</a>
                @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                @endif
                @if (session('delete'))
                    <div class="alert alert-danger">
                        {{session('delete')}}
                    </div>
                @endif
                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Atasan</th>
                        <th scope="col">Company</th>
                        <th scope="col">Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($employee as $emp)
                        <tr>
                            <th>{{$emp->id}}</th>
                            <td>{{$emp->nama}}</td>
                            <td>{{$emp->atasan_id}}</td>
                            <td>{{$emp->company_id}}</td>
                            <td>
                                <a href="employee/{{$emp->id}}/edit" class="btn btn-success">Edit</a>
                                <form action="employee/{{$emp->id}}" method="post" class="d-inline">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Hapus</button>
                                </form>
                            </td>
                        </tr>
                    </tbody>
                    @endforeach
                </table>
            </div>
        </div>
    </div>

@endsection

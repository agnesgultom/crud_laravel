@extends('organisasi.layout.main')

@section('title', 'Create Employee')

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-10">
                <h1 class="mt-3">Form Tambah Data Employee</h1>
                <form method="post" action="../employee">
                    @csrf
                    <div class="form-group">
                        <label for="id" class="form-label">ID</label>
                        <input type="number" class="form-control @error('id') is-invalid @enderror" id="id" placeholder="Masukkan ID Employee" name="id">
                        @error('id')<div class="invalid-feedback">{{$message}}</div>@enderror
                    </div>
                    <div class="form-group">
                        <label for="nama" class="form-label">Nama</label>
                        <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" placeholder="Masukkan Nama Employee" name="nama">
                        @error('nama')<div class="invalid-feedback">{{$message}}</div>@enderror
                    </div>
                    <div class="form-group">
                        <label for="id_atasan" class="form-label">ID Atasan</label>
                        <input type="number" class="form-control @error('atasan_id') is-invalid @enderror" id="id" placeholder="Masukkan ID Atasan Employee" name="atasan_id">
                        @error('atasan_id')<div class="invalid-feedback">{{$message}}</div>@enderror
                    </div>
                    <div class="form-group">
                        <label for="id_company" class="form-label">ID Company</label>
                        <input type="number" class="form-control @error('company_id') is-invalid @enderror" id="id_company" placeholder="Masukkan ID Company" name="company_id">
                        @error('company_id')<div class="invalid-feedback">{{$message}}</div>@enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection

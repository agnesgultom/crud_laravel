import React from 'react';
import ReactDom from 'react-dom';
const Navbar = () => {
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <a className="navbar-brand" href="#">Home</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav mr-auto">
                    <li className="nav-item active">
                        <a className="nav-link" href="#">Provinsi <span className="sr-only">(current)</span></a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#">Kota/Kabupaten</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#">Kecamatan</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#">Desa</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="/">Keluar</a>
                    </li>
                </ul>
            </div>
        </nav>
    );
}
export default Navbar;

import React from "react";
import Navbar from "./Navbar";
const AppContainer = ({title, children}) => {
    return(
        <div className="container">
            <Navbar/>
            <div className="card">
                <h5 className="card-aheader">
                    {title}
                </h5>
                <div className="card-body">
                    {children}
                </div>
            </div>
        </div>
    );
}
export default AppContainer;

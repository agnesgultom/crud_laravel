import React from 'react';
import ReactDom from 'react-dom';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from 'react-router-dom';
import Home from 'resources/js/src/wilayah/provinsi/Home';
import Add from 'resources/js/src/wilayah/provinsi/Add';
import Edit from "./components/Edit";
const ProvApp = () =>{
    return(
        <Router clasName = "App_container">
            <Switch>
                <Route exact path="/">
                    <Home/>
                </Route>
                <Route path="/add">
                    <Add/>
                </Route>
                <Route path="/edit/:id">
                    <Edit/>
                </Route>
            </Switch>
        </Router>
    );
};
ReactDom.render(<ProvApp/>, document.getElementById('app'));


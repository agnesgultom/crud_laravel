import React, {useEffect, useState} from "react";
import {Link} from "react-router-dom";
import AppContainer from "../../components/AppContainer";
import api from './api';
const Home = () => {
    const [post, setPost] = useState(null);
    const fetchPost = () =>{
        api.getAllPosts().then(res => {
            const result = res.data;
            setPost(result.data)
        });
    }

    useEffect(() => {
        fetchPost();
    }, [] );

    const renderPost = () => {
        if(!post){
            return (
                <tr>
                    <td colSpan="4">
                        Loading posts..........
                    </td>
                </tr>
            );
        }
        if(post.length === 0){
            return (
                <tr>
                    <td colSpan="4">
                        There is no post yet. Add one.
                    </td>
                </tr>
            );
        }
        return post.map((post) => (
            <tr>
                <td>{post.id}</td>
                <td>{post.title}</td>
                <td>{post.description}</td>
                <td>
                    <Link className="btn btn-warning" to={`/edit/${post.id}`}>Edit</Link>
                    <button
                        type="button"
                        className="btn btn-danger" o
                        onClick ={() => {
                            api.deletePost(post.id)
                                .then(fetchPost())
                                .catch(err => {
                                    alert('Failed to delete post with id : ' +post.id)
                                });
                        }}>Delete</button>
                </td>
            </tr>
        ))
    }

    return(
        <AppContainer title="Laravel ReactJS-CRUD">
            <Link to="/add" className="btn btn-primary">Add</Link>
            <div className="table-responsive">
                <table className="table table-stripped mt-4">
                    <thead>
                    <tr>
                        <th>ID.</th>
                        <th>Nama</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {renderPost()}
                    </tbody>
                </table>
            </div>
        </AppContainer>
    );
}
export default Home;

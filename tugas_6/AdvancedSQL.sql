--Jumlah Komplain Setiap Bulan
SELECT MONTH(date_received) AS Bulan, COUNT(complaint_id) AS 'Jumlah Komplain' 
FROM `consumer_complaint` 
GROUP BY MONTH(date_received)

--Komplain yang memiliki tags 'Older American'
SELECT * FROM `consumer_complaint` where tags="Older American"

--Jumlah company response to Consumer
SELECT company, 
    SUM(IF(company_response_to_consumer='closed',1,0)) AS Closed, 
    SUM(IF(company_response_to_consumer='closed with explanation',1,0)) AS 'Closed with explanation', 
    SUM(IF(company_response_to_consumer='closed with non-monetary relief',1,0)) AS 'Closed with non-monetary relief', 	
    SUM(IF(company_response_to_consumer='closed with monetary relief',1,0)) AS 'Closed with monetary relief', 
    SUM(IF(company_response_to_consumer='Untimely response',1,0)) AS 'Untimely response',
    COUNT(company_response_to_consumer)
from `consumer_complaint` group by company
create table karyawan(
    id int primary key,
    nama char(50),
    jenis_kelamin ENUM('L', 'P'),
    status ENUM('Menikah', 'Belum'),
    tanggal_lahir DATE,
    tanggal_masuk DATE,
    departemen int
);

create table departemen(
    id int primary key,
    nama char(50)
);

ALTER TABLE karyawan
ADD FOREIGN KEY (departemen) REFERENCES departemen(id);

INSERT INTO karyawan VALUES ( 1, 'Rizki Saputra', 'L', 'Menikah', '1980-10-11', '2011-1-1', 1),
                            ( 2, 'Farhan Reza', 'L', 'Belum', '1989-11-1', '2011-1-1', 1),
                            ( 3, 'Riyando Adi', 'L', 'Menikah', '1977-1-25', '2011-1-1', 1),
                            ( 4, 'Diego Manuel', 'L', 'Menikah', '1983-2-22', '2012-9-4', 2),
                            ( 5, 'Satya Laksana', 'L', 'Menikah', '1981-1-12', '2011-3-19', 2),
                            ( 6, 'Miguel Hernandez', 'L', 'Menikah', '1994-10-16', '2014-6-15', 2),
                            ( 7, 'Putri Persada', 'P', 'Menikah', '1988-1-30', '2013-4-14', 2),
                            ( 8, 'Alma Safira', 'P', 'Menikah', '1991-5-18', '2013-9-28', 3),
                            ( 9, 'Haqi Hafiz', 'L', 'Belum', '1995-9-19', '2015-3-9', 3),
                            ( 10, 'Abi Isyawara', 'L', 'Belum', '1991-6-3', '2012-1-22', 3),
                            ( 11, 'Maman Kresna', 'L', 'Belum', '1993-8-21', '2012-9-15', 3),
                            ( 12, 'Nadia Aulia', 'P', 'Belum', '1989-10-7', '2012-5-7', 4),
                            ( 13, 'Mutiara Rezki', 'P', 'Menikah', '1988-3-23', '2013-5-21', 4),
                            ( 14, 'Dani Setiawan', 'L', 'Belum', '1986-2-11', '2014-11-30', 4),
                            ( 15, 'Budi Putra', 'L', 'Belum', '1995-10-23', '2015-12-3', 4);
insert into departemen values (1, 'Manajemen'),
                              (2, 'Pengembang Bisnis'),
                              (3, 'Teknisi'),
                              (4, 'Analis');
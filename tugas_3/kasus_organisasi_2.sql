CREATE TABLE employee(
    id int primary key,
    nama char(50),
    atasan_id int,
    company_id int
);

create table company(
    id int primary key,
    nama char(50),
    alamat char(50)
);

alter table employee add foreign key (company_id) references company(id);

insert into company values (1, 'PT JAVAN', 'Slamen'),
                           (2, 'PT Dicoding', 'Bandung');

INSERT INTO employee values (1, 'Pak Budi', null , 1),
                            (2, 'Pak Tono' , 1, 1),
                            (3, 'Pak Totok', 1, 1),
                            (4, 'Bu Sinta', 2, 1),
                            (5, 'Bu Novi', 3, 1),
                            (6, 'Andre', 4, 1),
                            (7, 'Dono', 4, 1),
                            (8, 'Ismir', 5, 1),
                            (9, 'Anto', 5, 1);

SELECT nama as CEO from employee where atasan_id is null;

SELECT b.nama as Staff from employee a right join employee b
    on a.atasan_id=b.id where a.nama is null;

select a.nama as Direktur from employee a left join employee b
    on a.atasan_id=b.id where b.nama = (
    SELECT nama as CEO from employee where atasan_id is null);

SELECT a.id, a.nama, b.nama from employee a left join employee b
    on a.atasan_id = b.id;
SELECT a.id, a.nama, b.nama from employee a right join employee b
    on a.atasan_id = b.id;

select nama as Manager from employee where
    nama not in
        (SELECT b.nama as Staff from employee a right join employee b
        on a.atasan_id=b.id where a.nama is null)
    and nama not in
        (select a.nama as Direktur from employee a left join employee b
        on a.atasan_id=b.id where b.nama = ( SELECT nama as CEO from employee
        where atasan_id is null))
    and nama not in
        (SELECT nama as CEO from employee where atasan_id is null);

SELECT a.nama FROM employee a inner join employee e on a.atasan_id = e.id;
SELECT * from employee;
select count(nama) from employee a inner join employee b where atasan_id is null ;

DELIMITER //
CREATE PROCEDURE jumlahbawahan (nama_atasan varchar(50))
 BEGIN
    select @id := id from employee where nama=nama_atasan;
  select nama_atasan, count(id) as Jumlah_bawahan
    from (select * from employee
         order by atasan_id, id) sorted,
        (select @pv := @id ) initialisation
where   find_in_set(atasan_id, @pv) > 0
and     @pv := concat(@pv, ',', id);
 END;
//
DELIMITER ;
drop procedure jumlahbawahan;
call jumlahbawahan('Bu Sinta');



ALTER TABLE karyawan ADD atasan int;

UPDATE karyawan SET atasan = 1 where nama='Farhan Reza';
UPDATE karyawan SET atasan = 1 where nama='Riyando Adi';
UPDATE karyawan SET atasan= 2 where  departemen = 2;
UPDATE karyawan SET atasan= 2 where  departemen = 3;
UPDATE karyawan SET atasan= 3 where  departemen = 4;

SELECT a.nama
FROM karyawan as a left join karyawan b
    on a.atasan = b.id
where a.atasan = 2 or a.atasan = 3;